#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup()
{
    ofBackground(0);
    ofSetFrameRate(60);
    
    unsigned w = 5;
    unsigned h = 10;
    
    particles.init(w, h);
    
    // initial positions
    float* particlePosns = new float[w * h * 4];
    for (unsigned y = 0; y < h; ++y)
    {
        for (unsigned x = 0; x < w; ++x)
        {
            unsigned idx = y * w + x;
            particlePosns[idx * 4] = 500.f * x / (float)w - 250.f; // particle x
            particlePosns[idx * 4 + 1] = 10.f * y; //500.f * y / (float)h - 250.f; // particle y
            particlePosns[idx * 4 + 2] = 0.f; // particle z
            particlePosns[idx * 4 + 3] = 0.f; // dummy
        }
    }
    particles.loadDataTexture(ofxGpuParticles::POSITION, particlePosns);
    delete[] particlePosns;
    
    // initial velocities
    float* particleVels = new float[w * h * 4];
    for (unsigned i = 0; i < w * h; ++i)
    {
        particleVels[i * 4] = 0.f;//ofRandom(-2, 2);
        particleVels[i * 4 + 1] = 0.f;//ofRandom(-2, 2);
        particleVels[i * 4 + 2] = 0.f;//ofRandom(-2, 2);
        particleVels[i * 4 + 3] = 0.f;//ofRandom(-4e-3, -1e-3); // particle decay
    }
    particles.loadDataTexture(ofxGpuParticles::VELOCITY, particleVels);
    delete[] particleVels;
}

//--------------------------------------------------------------
void testApp::update()
{
    float particlePosns[particles.getWidth() * 4];
    for (unsigned i = 0; i < particles.getWidth(); ++i)
    {
        particlePosns[i * 4] = 10.f * i + 100.f * ofGetMouseX() / ofGetWidth() - 50.f;
        particlePosns[i * 4 + 1] = 0.f;
        particlePosns[i * 4 + 2] = 0.f;
        particlePosns[i * 4 + 3] = 0.f;
    }
    particles.loadDataTexture(ofxGpuParticles::POSITION, particlePosns, 0, 0, particles.getWidth(), 1);
    particles.update();
}

//--------------------------------------------------------------
void testApp::draw()
{
    cam.begin();
    particles.draw();
    cam.end();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}