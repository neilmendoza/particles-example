#version 120

// ping pong inputs
uniform sampler2DRect particles0;
uniform sampler2DRect particles1;

void main()
{
    vec4 pos = texture2DRect(particles0, gl_TexCoord[0].st);
    vec4 vel = texture2DRect(particles1, gl_TexCoord[0].st);
    
    vec4 parentPos = texture2DRect(particles0, vec2(gl_TexCoord[0].s, gl_TexCoord[0].t - 1.0));
    
    vec3 springForceDirection = (parentPos - pos).xyz;
    float springConstant = 0.1;
    float restLength = 10.0;
    
    float magnitude = springConstant * (length(springForceDirection) - restLength);
    
    vec4 force = vec4(normalize(springForceDirection), 0.0) * magnitude;
    force += vec4(0.0, 0.5, 0.0, 0.0);
    
    vel += step(0.5, gl_TexCoord[0].t) * force;
    vel *= 0.8;
    
    pos += step(0.5, gl_TexCoord[0].t) * vel;
    
    gl_FragData[0] = pos;
    gl_FragData[1] = vel;
}